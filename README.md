# WebServiceAPIs

GOMEZ Eve - GRAVE Léa - M1 DEVLMIOT - LE PROVOST Julien - M1 EXDEVOPS

> **Warning**
> La nouvelle documentation mis à jour et à suivre est la suivante.
> Les README internes aux différents dépôts ne sont plus à jour.

## Procédure à suivre pour récupérer le projet webserviceapis

1. Installer le repo global :
`git clone https://gitlab.com/2g1/webserviceapis.git`

2. Initialiser et mettre à jour les sous modules :
`git submodule update --init --recursive`

3. Changer de branche :
`git submodule foreach -q --recursive "git checkout develop"`


## Exécuter le projet webserviceapis

1. Se placer à la racine du repo, au même niveau que le docker-compose.yaml

2. Exécuter le docker compose : 
`docker compose up --build`

## Accès aux swagger

1. Accèder au swagger webservicefilmapi
`IP:3000/api-docs`

2. Accèder au swagger webserviceauthenticationmapi
`IP:8000/api-docs`

3. Accèder au swagger webservicereservationsapi
`IP:8081/api-docs`

## Connexion utilisateur

1. Pour se connecter en tant qu'administrateur, un compte administrateur par default a été crée :
- utilisateur : `michael.foraste@ynov.com`
- mot de passe : `michael`

> **Warning**
> Lors de l'utilisation du swagger, il ne faut pas oublier de rentrer le token de connexion au niveau du "Authorize" en haut à droit du swagger sinon cela ne fonctionnera pas.

![Authorize](./swagger_authorize.PNG "Authorize").

## Pour mettre à jour les submodules

1. Récupérer les nouveaux commits dans les submodules :
`git submodule foreach -q --recursive "git pull"`
